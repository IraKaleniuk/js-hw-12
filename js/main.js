"use strict";

const btnArr = Array.from(document.querySelectorAll(".btn"));

document.addEventListener("keyup", (e) => {
    const active = document.querySelector(".active");
    if(active) active.classList.remove("active");

    btnArr.forEach((btn) => {
        if(btn.dataset.code === e.code) btn.classList.add("active");
    })
})


